CROSS_PATH=$(HOME)/opt/cross/bin

LD=$(CROSS_PATH)/i686-elf-ld
CC=$(CROSS_PATH)/i686-elf-gcc

CFLAGS=-g -ffreestanding -falign-jumps -falign-functions -falign-labels \
	-falign-loops -fstrength-reduce -fomit-frame-pointer \
	-finline-functions -Wno-unused-function -fno-builtin -Werror \
	-Wno-unused-label -Wno-cpp -Wno-unused-parameter -nostdlib \
	-nostartfiles -nodefaultlibs -Wall -O0
INCLUDES=-Isys -I.

ASM=nasm
ECHO=echo -e
QEMU=qemu-system-i386

NO_SECTORS=100

OBJS=sys/kernel.asm.o sys/kernel.o sys/string/string.o

# Vesa Driver
OBJS +=drivers/vesa/vesa.o drivers/vesa/terminal.o drivers/vesa/fonts/vga.o

all: arch/i386/boot.bin sys/kernel.out
	@dd if=arch/i386/boot.bin > kinl.bin
	@dd if=sys/kernel.out >> kinl.bin
	@dd if=/dev/zero bs=512 count=$(NO_SECTORS) >> kinl.bin

sys/kernel.out: $(OBJS)
	@$(LD) -g -relocatable $(OBJS) -o sys/kernelfull.o
	@$(CC) $(CFLAGS) -T linker.ld -o $@ -ffreestanding -O0 -nostdlib sys/kernelfull.o

arch/i386/boot.bin: arch/i386/boot.asm
	@$(ECHO) "ASM\t\t"$<
	@$(ASM) -f bin $< -o $@

sys/kernel.asm.o: sys/kernel.asm
	@$(ECHO) "ASM\t\t"$<
	@$(ASM) -f elf -g $< -o $@

sys/kernel.o: sys/kernel.c
	@$(ECHO) "CC\t\t"$<
	@$(CC) $(CFLAGS) $(INCLUDES) -std=gnu99 -c $< -o $@

sys/string/string.o: sys/string/string.c
	@$(ECHO) "CC\t\t"$<
	@$(CC) $(CFLAGS) $(INCLUDES) -std=gnu99 -c $< -o $@

# Vesa Driver
drivers/vesa/vesa.o: drivers/vesa/vesa.c
	@$(ECHO) "CC\t\t"$<
	@$(CC) $(CFLAGS) $(INCLUDES) -std=gnu99 -c $< -o $@

drivers/vesa/terminal.o: drivers/vesa/terminal.c
	@$(ECHO) "CC\t\t"$<
	@$(CC) $(CFLAGS) $(INCLUDES) -std=gnu99 -c $< -o $@

drivers/vesa/fonts/vga.o: drivers/vesa/fonts/vga.c
	@$(ECHO) "CC\t\t"$<
	@$(CC) $(CFLAGS) $(INCLUDES) -std=gnu99 -c $< -o $@

run: all
	@$(QEMU) -hda kinl.bin

clean:
	@rm -Rf arch/i386/boot.bin kinl.bin sys/kernel.out sys/kernelfull.o $(OBJS)

%.asm: ;
